# PoshInteractive

This PowerShell module provides a set of handy pipeline tools for interactive sessions.

## Installation

```
Install-Module PoshInteractive -Scope Currentuser
```

## Usage
PoshInteractive can do stuff like an on demand progress indicator, pausing, stepping up and down the pipeline, and exiting cleanly from the pipeline.

    .EXAMPLE
    1..100 | iip | %{ sleep -mil 100 }

    # Press P or ? to pause execution and enter command mode

    .EXAMPLE
    1..100 | iip -ShowProgress -TotalCount 100 | %{ sleep -mil 100 }

    # Tells the command how many iterations to expect because that is an unknown in pipeline exection

    .EXAMPLE
    Interactive (1..100) -ShowProgress | %{ sleep -mil 100 }

    # Same as prior example, but can determine object count due to no pipeline

    .EXAMPLE
    Interactive (1..100) -ShowProgress -StatusMessage {"{0} - {1}" -f $args.Existing,$args.InputObject} | %{ sleep -mil 100 }

    Append the InputObject onto the Write-Progress Status parameter.

    .EXAMPLE
    Interactive (1..100) -ShowProgress -StatusMessage {"{0}/{1} - {2}" -f $args.CurrentCount,$args.TotalCount,$args.InputObject} | %{ sleep -mil 100 }

    Override the Write-Progress Status parameter with a custom output text

    .EXAMPLE
    Interactive (1..100) -ShowProgress | %{ sleep -mil 50 ;$_/2} | iip | measure -Average

    # Press P to pause, then C to show current object, then <Right> to move to the other Invoke-InteractivePipeline (Interactive/iip), then press C to show current object.
    # Notice that if you press N and then C, you are still only seeing the objects as they pass the second instance of iip in the pipeline.

## Support
Raise an issue here.

Alternatively, you can contact me directly from info on [my blog](https://blog.dcrich.net).

## Roadmap
Nothing currently.

## Contributing
Open a MR or post an issue.

## Authors and acknowledgment
Loosely based on [SeeminglyScience's StopUpstreamCommandsException Gist](https://gist.github.com/SeeminglyScience/7eb62053bd16dc4406ac6d9e7004fe75)

## License
MIT
