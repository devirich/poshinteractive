﻿function Invoke-InteractiveCommand {
    # This function gets run in the local scope via dot source:
    # It edits variables directly.
    do {
        if (-not [Console]::KeyAvailable) {
            Write-Host "Processing paused by function. Press any command key (?/ /s/#/p/e/,/./n/c/j/d/other):"
        }
        $keyInfo = [Console]::ReadKey($true)

        if ($keyInfo.KeyChar -eq '?') {
            @(
                "q - Quit"
                "<space> - Write-Progress"
                "s - Toggle Write-Progress"
                "# - Enter total count for Write-Progress"
                "p - Pause processing"
                "e - Enter nested prompt"
                "<Right>/. - Move active Interactive down the pipeline (to the right)"
                "<Left>/, - Move active Interactive up the pipeline (to the left)"
                "<Down>/n - Next item"
                "c - Current item display"
                "j - Current item display (JSON)"
                "d - Debug info"
                "? - This help text"
                "Anything else - continue processing"
            ) | Out-String | Write-Host
            $script:Paused = $true
        }
        elseif ($keyInfo.KeyChar -eq 'q') {
            Assert-CommandStopperInitialized
            [UtilityProfile.CommandStopper]::Stop($PSCmdlet)
        }
        elseif ($keyInfo.KeyChar -eq ' ') {
            $script:Paused = $false
            Write-ProgressReport
        }
        elseif ($keyInfo.KeyChar -eq 's') {
            if ($ShowProgress) { $ShowProgress = $false }
            else {
                $ShowProgress = $true
            }
        }
        elseif ($keyInfo.KeyChar -eq '#') {
            [int]$TotalCount = Read-Host "TotalCount"
        }
        elseif ($keyInfo.KeyChar -eq 'p') {
            Write-Host "Processing paused. Press any command key to run that command or any other key to continue..."
            # [void][Console]::ReadKey($true)
            $script:Paused = $true
        }
        elseif ($keyInfo.KeyChar -eq 'e') {
            Write-Host "use `$item, `$PSItem, or `$Inputobject to access current item"
            $host.EnterNestedPrompt()
        }
        elseif ($keyInfo.Key -in 37, 'OemComma') {
            $script:Paused = $false
            $script:continuePause = $true
            if ($script:activeID -gt 1) {
                $script:activeID--
            }
        }
        elseif ($keyInfo.Key -in 39, 'OemPeriod') {
            $script:Paused = $false
            $script:continuePause = $true
            if ($script:scheduler.$HistoryId -gt $script:activeID) {
                $script:activeID++
            }
        }
        elseif ($keyInfo.Key -in 40, 'n') {
            $script:Paused = $false
            $script:continuePause = $true
        }
        elseif ($keyInfo.KeyChar -eq 'c') {
            $InputObject | Out-String | Write-Host
        }
        elseif ($keyInfo.KeyChar -eq 'j') {
            if ($ENV:INTERACTIVE_JSON_DEPTH) { $jsonDepth = $ENV:INTERACTIVE_JSON_DEPTH }
            else { $jsonDepth = 2 }
            $InputObject | ConvertTo-Json -Depth $jsonDepth | Out-String | Write-Host
        }
        elseif ($keyInfo.KeyChar -eq 'd') {
            "Item number: {0}, value: {1}, interactives count: {2}, active invocation: {3}" -f @(
                $current
                $item
                $script:scheduler.$HistoryId
                $script:activeID
            ) | Write-Host
        }
        else {
            $script:Paused = $false
        }

    } while ($script:Paused)

    # Enable skipping out of current loop but stop next active occurence.
    if ($script:continuePause) {
        $script:Paused = $true
        $script:continuePause = $false
    }
}
