function Invoke-InteractivePipeline {
    <#
    .SYNOPSIS
    This PowerShell function provides a set of handy pipeline tools for interactive sessions.

    .DESCRIPTION
    This tool can do stuff like an on demand progress indicator, pausing pipeline, stepping up and down the pipeline, and exiting pipeline cleanly

    .PARAMETER InputObject
    The pipeline object to send on downstream

    .PARAMETER TotalCount
    The number of total items to be iterated over. This can be used to calculate SecondsLeft and PercentComplete for Write-Progress

    .PARAMETER Pause
    Begin by starting at the command screen

    .PARAMETER ShowProgress
    Enable updating via Write-Progress for each object that is iterated over

    .PARAMETER StatusMessage
    A custom scriptblock to run when showing progress. Some parameters are passed into the scriptblock via $args variable.

    Available parameters in $args are:
    Existing: The default status message
    InputObject: The current item loaded in the pipeline
    TotalCount: The number of expected objects down the pipeline

    .EXAMPLE
    1..100 | iip | %{ sleep -mil 100 }

    # Press P or ? to pause execution and enter command mode

    .EXAMPLE
    1..100 | iip -ShowProgress -TotalCount 100 | %{ sleep -mil 100 }

    # Tells the command how many iterations to expect because that is an unknown in pipeline exection

    .EXAMPLE
    Interactive (1..100) -ShowProgress | %{ sleep -mil 100 }

    # Same as prior example, but can determine object count due to no pipeline

    .EXAMPLE
    Interactive (1..100) -ShowProgress -StatusMessage {"{0} - {1}" -f $args.Existing,$args.InputObject} | %{ sleep -mil 100 }

    Append the InputObject onto the Write-Progress Status parameter.

    .EXAMPLE
    Interactive (1..100) -ShowProgress -StatusMessage {"{0}/{1} - {2}" -f $args.CurrentCount,$args.TotalCount,$args.InputObject} | %{ sleep -mil 100 }

    Override the Write-Progress Status parameter with a custom output text

    .EXAMPLE
    Interactive (1..100) -ShowProgress | %{ sleep -mil 50 ;$_/2} | iip | measure -Average

    # Press P to pause, then C to show current object, then <Right> to move to the other Invoke-InteractivePipeline (Interactive/iip), then press C to show current object.
    # Notice that if you press N and then C, you are still only seeing the objects as they pass the second instance of iip in the pipeline.

    .NOTES
    This function unrolls any input arrays! You have been warned.

    #>
    [Alias('iip', 'interactive')]
    # [CmdletBinding(PositionalBinding = $false)]
    [CmdletBinding()]
    param(
        [Parameter(ValueFromPipeline)]
        $InputObject,

        # [Parameter(Position = 0)]
        [ValidateRange(1, [int]::MaxValue)]
        [int] $TotalCount,

        [switch] $Pause,
        [switch] $ShowProgress,

        [ScriptBlock]$StatusMessage
    )
    begin {
        function Write-ProgressReport {
            $write_progress_splat.Status = "Currently on item #$current"
            if ($TotalCount) {
                $write_progress_splat.Status += " (of $TotalCount)"
                $write_progress_splat.PercentComplete = $current / $TotalCount * 100
                if ($write_progress_splat.PercentComplete -gt 100) {
                    $write_progress_splat.PercentComplete = 100
                    $write_progress_splat.SecondsRemaining = 0
                }
                else {
                    $write_progress_splat.SecondsRemaining = ($timer.Elapsed / ($current / $TotalCount) - $timer.Elapsed + $timer.Elapsed / $current).TotalSeconds
                    $timeleft = [timespan]::new(0, 0, $write_progress_splat.SecondsRemaining)
                    $time_string = ""
                    if ($timeleft.Days) { $time_string += "{0}d" -f $timeleft.Days }
                    if ($timeleft.Hours) { $time_string += "{0}h" -f $timeleft.Hours }
                    if ($timeleft.Minutes) { $time_string += "{0}m" -f $timeleft.Minutes }
                    if ($timeleft.Seconds) { $time_string += "{0}s" -f $timeleft.Seconds }
                    $write_progress_splat.Status += " (remaining:$time_string)"
                }
            }
            if ($StatusMessage) {
                $write_progress_splat.Status = (
                    Invoke-Command -ScriptBlock $StatusMessage -ArgumentList @{
                        Existing     = $write_progress_splat.Status
                        InputObject  = $item
                        CurrentCount = $current
                        TotalCount   = $TotalCount
                    }
                )
            }
            Write-Progress @write_progress_splat
        }

        if ($InputObject) { $TotalCount = $InputObject.count }
        $current = 0
        $write_progress_splat = @{
            Activity = "Processing pipeline objects"
        }
        if (-not $script:activeID) { $script:activeID = 1 }
        if (-not $script:scheduler) { $script:scheduler = @{} }

        # Use a hashtable with one key per command to ensure stuff is reset for each command
        $HistoryId = $MyInvocation.HistoryId
        $script:scheduler.$HistoryId++
        $myID = $script:scheduler.$HistoryId
        # First occurance of this in a pipeline should clear any leftover pauses
        if ($myID -eq 1) { $script:Paused = $false }
        # If this occurance should start paused, set to paused
        if ($Pause) { $script:Paused = $true }

        $timer = [System.Diagnostics.Stopwatch]::StartNew()
    }

    process {
        foreach ($item in $InputObject) {
            $current++

            if ([Console]::KeyAvailable -or $script:Paused) {
                $timer.Stop()
                if ($myID -eq $script:activeID) {
                    . Invoke-InteractiveCommand
                    while ([Console]::KeyAvailable) { [void][Console]::ReadKey($true) }
                }
                $timer.Start()
            }
            elseif ($ShowProgress) {
                Write-ProgressReport
            }

            $item
        }
    }

    end {
        if ($script:scheduler.$HistoryId -lt $script:activeID) {
            $script:activeID = 1
        }
    }
}
